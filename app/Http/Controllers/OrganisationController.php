<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Organisation;
use App\Services\OrganisationService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\OrganisationRequest;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;
use App\Mail\OrganisationCreatedNotificationMail;

/**
 * Class OrganisationController
 * @package App\Http\Controllers
 */
class OrganisationController extends ApiController
{
	/**
	 * @param OrganisationService $service
	 * @return JsonResponse
	 */
	public function index(OrganisationService $service): JsonResponse
	{
		/** @var Organisation[] $organisations */
		$organisations = $service->loadAll($this->request->get('filter', 'all'));

		return $this
			->transformCollection('organisation', $organisations, ['user'])
			->respond();
	}

	/**
	 * @param OrganisationService $service
	 * @param OrganisationRequest $organisationRequest
	 * @return JsonResponse
	 */
    public function store(OrganisationService $service, OrganisationRequest $organisationRequest): JsonResponse
    {
		/** @var Organisation $organisation */
        $organisation = $service->createOrganisation($organisationRequest->all());

        //Sending email to notify the user
        Mail::to(Auth::user())->send(new OrganisationCreatedNotificationMail($organisation));

        return $this
            ->transformItem('organisation', $organisation, ['user'])
            ->respond();
    }
}
