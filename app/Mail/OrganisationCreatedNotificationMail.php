<?php

declare(strict_types=1);

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Organisation;

class OrganisationCreatedNotificationMail extends Mailable
{
    use Queueable, SerializesModels;

	/**
	 * @var Organisation
	 */
	protected $organisation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Organisation $organisation)
    {
    	$this->organisation = $organisation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
    	$organisationName = $this->organisation->name;
        return $this->view('mail.organisation_created_notification', compact('organisationName'));
    }
}
