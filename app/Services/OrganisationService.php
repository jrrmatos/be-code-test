<?php

declare(strict_types=1);

namespace App\Services;

use App\Organisation;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class OrganisationService
 * @package App\Services
 */
class OrganisationService
{
    /**
     * @param array $attributes
     *
     * @return Organisation
     */
    public function createOrganisation(array $attributes): Organisation
    {
        $organisation = Auth::user()->organisations()->create($attributes);

        return $organisation;
    }

    public function loadAll(string $filter = 'all'): Collection
	{
		return Organisation::when($filter === 'subbed', function ($q) {
			$q->where('subscribed', true);
		})
			->when($filter === 'trail', function ($q) {
				$q->where('subscribed', false);
			})
			->get();
	}
}
