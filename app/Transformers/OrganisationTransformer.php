<?php

declare(strict_types=1);

namespace App\Transformers;

use App\Organisation;
use League\Fractal\TransformerAbstract;

/**
 * Class OrganisationTransformer
 * @package App\Transformers
 */
class OrganisationTransformer extends TransformerAbstract
{
	/**
	 * List of includes
	 *
	 * @var string[]
	 */
	protected $availableIncludes = [
		'user'
	];

    /**
     * @param Organisation $organisation
     *
     * @return array
     */
    public function transform(Organisation $organisation): array
    {
        return [
			'id' => (int)$organisation->id,
        	'name' => $organisation->name,
			'subscribed' => $organisation->subscribed,
			'trial_end' => $organisation->trial_end->timestamp ?? null,
		];
    }

    /**
     * @param Organisation $organisation
     *
     * @return \League\Fractal\Resource\Item
     */
    public function includeUser(Organisation $organisation)
    {
        return $this->item($organisation->owner, new UserTransformer());
    }
}
